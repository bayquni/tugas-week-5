'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Sepedas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      merk: {
        allowNull: false,
        type: Sequelize.STRING
      },
      type: {
        allowNull: false,
        type: Sequelize.STRING
      },
      hargaBeli: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      stok: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      updatedBy: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references : {
          model :  "Admins",
          target : "id"
        }
      },
      vendorId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references : {
          model :  "Vendors",
          target : "id"
        }
      },
      adminId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references : {
          model :  "Vendors",
          target : "id"
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Sepedas');
  }
};