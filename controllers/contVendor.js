const { Vendor, Admin } = require('../models');

class contVendor {
    static async vendorRegister(req,res) {
        
        try {
            const dataVendor = await Vendor.create({
                contactPerson: req.body.contactPerson,
                telepon: req.body.telepon,
                alamat: req.body.alamat,
                // updateBy: 1

            })

            res.status(201).json({
                msg: `Vendor Berhasil Terdaftar`, dataVendor
            })

        }

        catch (err) {
            res.status(500).json({msg: `Vendor Gagal Terdaftar`, err, 
            error: err.message
            })
        }
    }

    static async vendorList(req,res) {
        try {
            const dataVendor = await Vendor.findAll()
            if (!dataVendor) {
            res.status(404).json({msg: `Belum Ada Vendor`})
            }

            res.status(200).json(dataVendor);
        }

        catch (err) {
            res.status(500).json({
                msg: `Error Mengambil Data`, err,
                error: err.message
                })
        }
    }

    static async vendorEdit(req,res) {
        try {
            const dataVendor= await Vendor.update(req.body, {
                where: {
                    id: +req.params.id,
                }
            })

            if (!dataVendor) return res.status(404).json({
                msg: `Vendor Tidak Ditemukan`
            });

            res.status(201).json({
                msg: `Vendor Berhasil di Update`
            });
        }
            catch (err) {
            res.status(500).json({
                msg: `Server Error`
            })
            }

        }

    static async vendorDelete(req, res) {
        try {
            const dataVendor= await Vendor.destroy({
                where: {
                    id: +req.params.id,
                }
            })

            if (!dataVendor) return res.status(404).json({
                msg: `Vendor Tidak Ditemukan`
            });

            res.status(201).json({
                msg: `Vendor Berhasil di Hapus`
            });
        
        }
        
        catch (err) {
            console.log(err)
        res.status(500).json({
        msg: `Server Error`
        })
            }
        }

    }
module.exports = contVendor;