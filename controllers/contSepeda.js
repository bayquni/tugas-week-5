const req = require('express/lib/request');
const res = require('express/lib/response');
const { Sepeda } = require('../models')

class constSepeda {
    static async sepedaPost(req, res) {
        try {
        const dataSepeda = await Sepeda.create(req.body);
            res.status(201).json({
            msg: `Sepeda Berhasil Terdata`, dataSepeda
        });
        } 
        
        catch (err) {
            res.status(500).json({
            msg: `Error Input Sepeda`
    });
        }
    }

    static async sepedaList(req, res) {
        try {
            const dataSepeda = await Sepeda.findAll()
            if (!dataSepeda) {
            res.status(404).json({msg: `Belum Ada Sepeda`})
            }

            res.status(200).json(dataSepeda);
        }

        catch (err) {
            console.log(err)
            res.status(500).json({
                msg: `Error Mengambil Data`
                // error: err.message
                })
        }
    }

    static async sepedaEdit(req, res) {
        try {
            const dataSepeda = await Sepeda.update(req.body, {
                where: {
                    id: req.params.id
                }
            })

            if (!dataSepeda) return res.status(404).json({
                msg: `Data Sepeda Tidak Ditemukan`
            })

            res.status(201).json({
                msg: `Data Sepeda Diperbaharui`
            })
        }

        catch (err) {
            res.status(500).json({
                msg: `Error Memperbaharui Data Sepeda`
            })
        }
    }

    static async sepedaHapus(req, res) {
        try {
            const dataSepeda = await Sepeda.destroy({
                where: {
                    id : +req.params.id
                }
            })

            if (!dataSepeda) return res.status(404).json({
                msg: `Data Sepeda Tidak Ditemukan`
            })

            res.status(201).json({
                msg: `Data Sepeda Berhasil Dihapus`
            })
        }

        catch (err) {
            res.status(500).json({
                msg: `Erro Penghapusaan Data Sepeda`
            })


        }
    }


}

module.exports = constSepeda;