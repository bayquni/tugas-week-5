const { Admin } = require('../models');
const { comparePassword, generateToken, hashPassword } = require('../middlewares/auth');

class contAdmin {
    static async adminRegister(req, res) {
        try {
            const data = await Admin.create({
                nama: req.body.nama,
                email: req.body.email,
                pass:  req.body.pass
            });
            return res.status(201).json({
                msg: `Registrasi Berhasil`, data
            });
        } 
        catch (err) {
            console.log(err)
            res.status(500).json({
                msg: `Registrasi Gagal`,
                error: err.message
            });
        }
    }

    static async adminLogin(req, res) {
        // console.log(req.body)
        // const { email, pass } = req.body;
        try {
            const data = await Admin.findOne({
                where: {
                    email: req.body.email
                }
            });
            
            console.log(data.pass)
            const cekPassword = comparePassword(req.body.pass, data.pass)
            if (!data || !cekPassword) 
            return res.status(404).json({
                msg: `E-mail/Password Salah`
            });

            let payload = {
                id: data.id,
                nama: data.nama,
                email: data.email
            };
            
            let token = generateToken(payload);

            res.status(200).json({ token });
            
        }   

        catch (err) {
            console.log(err)
            res.status(500).json({
                
                msg: `Server Error`,
                error: err.message
            });
        }
    }


}

module.exports = contAdmin;