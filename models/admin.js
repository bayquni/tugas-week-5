'use strict';
const {
  Model
} = require('sequelize');
const { hashPassword } = require('../middlewares/auth');
module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Admin.hasMany(models.Sepeda, {foreignKey: "updatedBy"})
    }
  };
  Admin.init({
    nama: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Nama Harus Diisi"
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "E-mail Harus Diisi"
        }
      }
    },
    pass: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [4,8],
          msg: `Password Harus 4 - 8 Karakter`
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Admin',
    hooks: {
      beforeCreate: (admin5) => {
        admin5.pass = hashPassword(admin5.pass); //sebelum dimasukan kje dnb dihash dulu
      }
    }
  });
  return Admin;
};