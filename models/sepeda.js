'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sepeda extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Sepeda.belongsTo(models.Vendor),
      Sepeda.belongsTo(models.Admin)
    }
  };
  Sepeda.init({
    merk: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Merk Harus Diisi"
        }
      }
    },
    type: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Type Harus Diisi"
        }
      }
    },
    hargaBeli: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: "Harga Harus Diisi"
        }
      }
    },
    stok: {
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: {
          args: true,
          msg: "Stok Harus Diisi"
        }
      }
    },
    updatedBy: DataTypes.INTEGER,
    vendorId: DataTypes.INTEGER,
    adminId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Sepeda',
  });
  return Sepeda;
};