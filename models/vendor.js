'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vendor extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Vendor.hasMany(models.Sepeda, {foreignKey: "vendorId"})
    }
  };
  Vendor.init({
    contactPerson: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Nama Kontak Harus Diisi"
        }
      }
    },
    telepon:{
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Nomor Telepon Harus Diisi"
        }
      }
    },
    alamat: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Alamat Harus Diisi"
        }
      }
    },
    updatedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Vendor',
  });
  return Vendor;
};