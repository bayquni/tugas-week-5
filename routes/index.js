const express = require("express");
const router = express.Router();
const Admin = require('./routesAdmin');
const Sepeda = require('./routesSepeda');
const Vendor = require('./routesVendor');


router.use("/admin", Admin);
router.use("/sepeda", Sepeda);
router.use("/vendor", Vendor);

module.exports = router;
