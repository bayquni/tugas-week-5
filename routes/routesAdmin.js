const express = require("express");
const router = express.Router();
const contAdmin  = require('../controllers/contAdmin')

router.post("/register", contAdmin.adminRegister);
router.post("/login", contAdmin.adminLogin);

module.exports = router;