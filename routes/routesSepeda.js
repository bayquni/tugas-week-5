const express = require("express");
const router = express.Router();
const contSepeda = require('../controllers/contSepeda')
const { authenticate } = require("../middlewares/auth");

router.post('/', authenticate, contSepeda.sepedaPost);
router.get('/', authenticate, contSepeda.sepedaList);
router.put('/:id', authenticate, contSepeda.sepedaEdit);
router.delete('/:id', authenticate, contSepeda.sepedaHapus);
module.exports = router;