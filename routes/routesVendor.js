const express = require("express");
const router = express.Router();
const contVendor = require('../controllers/contVendor')
const { authenticate } = require("../middlewares/auth");


router.post('/', authenticate, contVendor.vendorRegister);
router.get('/', authenticate, contVendor.vendorList);
router.put('/:id', authenticate, contVendor.vendorEdit);
router.delete('/:id', authenticate, contVendor.vendorDelete);
module.exports = router;