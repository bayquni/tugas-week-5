const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { Admin } = require("../models");

function hashPassword(password) {
  const hash = bcrypt.hashSync(password, 10);
  return hash;
}

function comparePassword(password, hash) {
  const compare = bcrypt.compareSync(password, hash);
  return compare;
}

function generateToken(payload) {
  const generate = jwt.sign(payload, process.env.SALT_JWT);
  return generate;
}

function verifyToken(token) {
  const verify = jwt.verify(token, process.env.SALT_JWT);
  return verify;
}

async function authenticate(req, res, next) {
  try {
    
    const token = req.headers.access_token;
    
    const decoded = verifyToken(token);
    // console.log(decoded.id)
    // const data = await Admin.findOne({
    //   where: {
    //     email: decoded.email
    
    //   },
    // });
    
    //     // req.admin = {id: admin.id};
    
    // if (!data) return res.status(401).json({ msg: "Tidak Ada Akses" });

    
    
    next();
  }
  
  catch (err) {
    res.status(500).json(err);
  }
}

module.exports = {
  authenticate,
  hashPassword,
  comparePassword,
  generateToken,
};
