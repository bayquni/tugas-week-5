const express = require('express');
const app = express();
const port = 5888;
const router = require('./routes');
// const cors = require('cors')

require("dotenv").config();

//* body parser 
app.use(express.json())
app.use(express.urlencoded({extended:true}));
app.use(router)
// app.use(cors())


app.listen(port, () => {
    console.log(`JALAN! ${port}`)
});

